module.exports = function (pool, tweet) {
    return {
        
        async create(ctx) {
            const getNoti = ctx.request.body
            // TODO: validate getNoti
            
            const id = await tweet.create(pool, getNoti)
            ctx.body = {
                getNoti
            }
        },
        
        async markAsRead(ctx) {
            const id = ctx.params.id
            let getTodo = await tweet.find(pool, id)
            ctx.body = getTodo
            // TODO: validate id
            // find todo from repo
            // send todo
        },        
       
    }
  }
  