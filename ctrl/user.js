module.exports = function(pool, userRepo) {
  return {
    async create(ctx) {
      const createUser = ctx.request.body;
      // TODO: validate createUser
      console.log(createUser);
      const id = await userRepo.create(pool, createUser)
      ctx.body = {
        createUser
      };
    }, 
    async getUser(ctx) {
      ctx.body = await todoRepo.list(pool)
    },

    async changePhoto(ctx) {
      const id = ctx.params.id
      const photo = ctx.request.body
      console.log(id)
      console.log(photo)      
      let getPhoto = await userRepo.patchPhoto(pool, photo['photo'] , id)      
      ctx.body = photo
    
    },
    async changeCover(ctx) {
      const id = ctx.params.id
      const cover = ctx.request.body
      let getCover = await userRepo.patchCover(pool, cover['cover'], id);
      ctx.body = cover;
      
    },
    async follow(ctx) {
      const createFollow = ctx.request.body;
      console.log(createFollow)
      let getFollow = await userRepo.doFollow(pool, createFollow)
     
      ctx.body = createFollow
    },
    async unfollow(ctx) {
      const id = ctx.params.id;
      const creteUnfollow = ctx.request.body
      let getUnlike = await userRepo.doUnfollow(pool, creteUnfollow);
      ctx.body = creteUnfollow
    },
    async chat(ctx) {
      const createChat = ctx.request.body;
      // TODO: validate createChat
      console.log(createChat);
      const getChat = await userRepo.doChat(pool, createChat)
      ctx.body = createChat      
    },

    async signUp(ctx) {
      const createSignUp = ctx.request.body
      console.log(createSignUp)
      const getSignUp = await userRepo.doSignUp(pool, createSignUp)
      ctx.body = createSignUp
    }
    
    
  };
};
