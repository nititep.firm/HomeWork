module.exports = {
    create,
    doAddChoice,
    vote
  }
  
  async function create (db, tweetId) {
    await db.execute(`
      insert into tweet_polls (
        tweet_id
      ) values (
        ?
      )
    `, [
      tweetId
    ])
  }
  
  async function doAddChoice (db, poll) {
    const result = await db.execute(`
      insert into tweet_polls (tweet_id ,content) 
      values (?, ?)
    `, [
      poll.tweet_id, poll.content
    ])
    return result[0].insertId
  }
  
  async function vote (db, userId, tweetId, choiceId) {
    await db.execute(`
      insert into tweet_choices (
        user_id, tweet_id, choice_id
      ) values (
        ?, ?, ?
      )
    `, [
      userId, tweetId, choiceId
    ])
  }